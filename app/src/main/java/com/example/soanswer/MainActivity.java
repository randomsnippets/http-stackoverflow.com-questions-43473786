package com.example.soanswer;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.chrisbanes.photoview.PhotoView;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends Activity {


    PhotoView pv;
    ImageView iv;
    Drawable d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pv = (PhotoView) findViewById(R.id.view_under_review);
        iv = (ImageView) findViewById(R.id.view_result);
        d = getDrawable(R.drawable.photo);

        pv.setImageDrawable(d);

        Button b = (Button) findViewById(R.id.go);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Drawable extracted = pv.getDrawable();
                Bitmap bm = ((BitmapDrawable)extracted).getBitmap();
                if(extracted == d){
                    // The drawable retrieved from the PhotoView is the exact same drawable as the
                    // one originally created from the source file
                    Toast.makeText(MainActivity.this,"Drawables are the same",Toast.LENGTH_SHORT).show();
//                    LoadDrawableIntoNewView(extracted);
                    LoadBitmapIntoNewView(bm);
                } else {

                    Toast.makeText(MainActivity.this,"Drawables differ",Toast.LENGTH_SHORT).show();

                }
            }
        });

    }

    private void LoadDrawableIntoNewView(Drawable extracted) {
        iv.setImageDrawable(extracted);
    }

    private void LoadBitmapIntoNewView(Bitmap ex) {
        iv.setImageBitmap(ex);
    }
}
